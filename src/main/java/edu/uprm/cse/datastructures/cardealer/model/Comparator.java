package edu.uprm.cse.datastructures.cardealer.model;

public interface Comparator<E> {

    public int compare(E obj1, E obj2);
}
