package edu.uprm.cse.datastructures.cardealer.model;

public class CarComparator<E> implements Comparator<E> {

    /**
     * This method is in charge of comparing cars based on different car specifications.
     * The order of spec-checking is important, it goes as follows:
     *
     *          (Car Brand -> Car Model -> Car Model options)
     *
     * The first specification to be evaluated is if the brands of the cars are the same or not.
     * If they are the same evaluate whether the car models are the same or not.
     * If the car models are the same as well, finally check whether the car model options
     * are the same or not. If they are all the same return 0.
     * If any of the spec above is different than the one it is begin compared to.
     * Return a positive number if the first car (@param obj1) spec begin evaluated (i.e car brand)
     * is less, in lexicographic terms, return a negative number. Otherwise, return a positive number.
     *
     *
     * @param obj1  car to compare
     * @param obj2 car to compare
     * @return     0, positive number or a negative number
     */
    @Override
    public int compare(E obj1, E obj2) {
        if (obj1 instanceof Car && obj2 instanceof Car) {
            int brandCompare = ((Car) obj1).getCarBrand().compareTo(((Car) obj2).getCarBrand());
            int modelCompare = ((Car) obj1).getCarModel().compareTo(((Car) obj2).getCarModel());
            int modelOptionsCompare = ((Car) obj1).getCarModelOption().compareTo(((Car) obj2).getCarModelOption());
            if (brandCompare == 0) {
                if (modelCompare == 0) {
                    return ((modelOptionsCompare == 0) ? modelCompare: modelOptionsCompare);
                }
                return modelCompare;
            }
            return brandCompare;
        }
        else {
            throw new ClassCastException("Objects were not of the same instance.");
        }
    }
}
