package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Arrays;

// Car class pojo
@XmlRootElement
public class Car implements Comparable<Car> {

	@XmlElement
	private final long carId;
	@XmlElement
	private final String carBrand;
	@XmlElement
	private final String carModel;
	@XmlElement
	private final String carModelOption;
	@XmlElement
	private final double carPrice;

	private static final CircularSortedDoublyLinkedList<Car> carList = new CircularSortedDoublyLinkedList<>();

	private Car(CarBuilder builder){
		this.carId = builder.carId;
		this.carBrand = builder.carBrand;
		this.carModel = builder.carModel;
		this.carModelOption = builder.carModelOption;
		this.carPrice = builder.carPrice;
	}

	public Car(){
		Car car = new Car.CarBuilder().carId(getCarId()).build();
		this.carId = car.getCarId();
		this.carBrand = car.getCarBrand();
		this.carModel = car.getCarModel();
		this.carModelOption = car.getCarModelOption();
		this.carPrice = car.getCarPrice();
	}

	public Car(long carId, String carBrand, String carModel,
					String carModelOption, double carPrice){
		Car car = new Car.CarBuilder().carId(carId)
				.carBrand(carBrand)
				.carModel(carModel)
				.carModelOption(carModelOption)
				.carPrice(carPrice)
				.build();
		this.carId = car.getCarId();
		this.carBrand = car.getCarBrand();
		this.carModel = car.getCarModel();
		this.carModelOption = car.getCarModelOption();
		this.carPrice = car.getCarPrice();
	}

	public long getCarId(){
		return this.carId;
	}

	public String getCarBrand() {
		return this.carBrand;
	}

	public String getCarModel() {
		return this.carModel;
	}

	public String getCarModelOption(){
		return this.carModelOption;
	}

	public double getCarPrice() {
		return this.carPrice;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((carBrand == null) ? 0 : carBrand.hashCode());
		result = prime * result + (int) (carId ^ (carId >>> 32));
		result = prime * result + ((carModel == null) ? 0 : carModel.hashCode());
		result = prime * result + ((carModelOption == null) ? 0 : carModelOption.hashCode());
		long temp;
		temp = Double.doubleToLongBits(carPrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Car other = (Car) obj;
		if (carBrand == null) {
			if (other.carBrand != null)
				return false;
		} else if (!carBrand.equals(other.carBrand))
			return false;
		if (carId != other.carId)
			return false;
		if (carModel == null) {
			if (other.carModel != null)
				return false;
		} else if (!carModel.equals(other.carModel))
			return false;
		if (carModelOption == null) {
			if (other.carModelOption != null)
				return false;
		} else if (!carModelOption.equals(other.carModelOption))
			return false;
		if (Double.doubleToLongBits(carPrice) != Double.doubleToLongBits(other.carPrice))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "{" + "\"carId\":" + carId + ", \"carBrand\":" + "\"" + carBrand + "\"" + ", \"carModel\":"
				+ "\"" + carModel + "\""  + ", \"carModelOption\":" + "\"" + carModelOption
				+ "\""  + ", \"carPrice\":" + carPrice + "}";

	}

	@Override
	public int compareTo(Car o) {
		return 0;
	}


	public static class CarBuilder{
		private long carId;
		private String carBrand = "";
		private String carModel = "";
		private String carModelOption = "";
		private double carPrice;

		public CarBuilder carId(long carId){
			this.carId = carId;
			return this;
		}

		public CarBuilder carBrand(String carBrand){
			this.carBrand = carBrand;
			return this;
		}

		public CarBuilder carModel(String carModel){
			this.carModel = carModel;
			return this;
		}

		public CarBuilder carModelOption(String carModelOption){
			this.carModelOption = carModelOption;
			return this;
		}

		public CarBuilder carPrice(double carPrice){
			this.carPrice = carPrice;
			return this;
		}

		public Car build(){
			return new Car(this);
		}

	}

	/**
	 *
	 * @return the sorted doubly linked list with all the current cars
	 */
	public static CircularSortedDoublyLinkedList<Car> getCarList() {
		return carList;
	}

	/**
	 * Get then car list in a string format.
	 * @return a list of all the cars currently on the list on a String format
	 */
	public static String getInstance(){
		return Arrays.toString(carList.toArray());
	}
}
