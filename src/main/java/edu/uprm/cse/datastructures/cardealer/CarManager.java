package edu.uprm.cse.datastructures.cardealer;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * This class is in charge of managing all MAVEN operations.
 */
@Path("/cars")
public class CarManager {

    public CarManager() {
    }


    /**
     *
     * @return all the cars currently on the doubly linked list
     */
    private String getCarList() {
        return Car.getInstance();
    }

    private final CircularSortedDoublyLinkedList<Car> linkedList = Car.getCarList();


    /**
     * Reads all the cars and return the information of all the cars as a JSON String.
     *
     * @return the information of all the cars in the list
     */
    @GET
    @Produces(APPLICATION_JSON)
    public String getAllCars() {
        return this.getCarList();
    }

    /**
     * Reads and returns the information of the car that have the same car id as the parameter.
     *
     * @param carId the car id that identifies the wanted car
     * @return      the car information that matches the given car id
     */
    @GET
    @Path("{id}")
    @Produces(APPLICATION_JSON)
    public Car getCar(@PathParam("id") long carId){
        for (int i = 0; i < linkedList.size(); i++) {
            if (linkedList.get(i).getCarId() == carId) {
                return linkedList.get(i);
            }
        }
        throw new NotFoundException(String.valueOf(new JsonError("Error", " Customer " + carId + " not found")));
    }


    /**
     * REST POST operation in charge of adding cars into the system.
     *
     * @param car to be added to the system.
     * @return status
     */
    @POST
    @Path("/add")
    @Produces(APPLICATION_JSON)
    public Response addCar(Car car) {
        linkedList.add(car);
        return Response.status(201).build();
    }

    /**
     * Basically it removes the car with the same car id as the one begin passed as parameter
     * and the add the new car to the list. This way the new car will be sorted on the list.
     * Receives a car and returns the car with the new updated information.
     *
     * @param car the car with all it's information
     * @return    the car with the updated information
     */
    @PUT
    @Path("{id}/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCar(Car car){
        // TODO: If you update you must update the list order with add or soomething.
        for (int i = 0; i < linkedList.size(); i++) {
            if (linkedList.get(i).getCarId() == car.getCarId()) {
                linkedList.remove(i);
                linkedList.add(car);
                return Response.status(Response.Status.OK).build();
            }
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    /**
     * Deletes the car that matches the car id given as parameter.
     * Returns the list of all the cars except the car deleted.
     *
     * @param carId the card id that identifies a car
     * @return      the information of all cars in the list minus the car deleted here
     */
    @DELETE
    @Path("/{id}/delete")
    public Response deleteCar(@PathParam("id") long carId){
        for (int i = 0 ; i < linkedList.size(); i++) {
            if (linkedList.get(i).getCarId() == carId) {
                linkedList.remove(i);
                return Response.status(Response.Status.OK).build();
            }
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    private static class JsonError {
        private String type;
        private String message;
        public JsonError(String type, String message) {
            this.type = type;
            this.message = message;
        }

        public String getType() {
            return this.type;
        }

        public String getMessage() {
            return this.message;
        }
    }


}
