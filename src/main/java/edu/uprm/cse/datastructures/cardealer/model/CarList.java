package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {

    /**
     * Clears all the cars currently on the list. Leave you with a empty car list.
     */
    public static void resetCars() {
        CircularSortedDoublyLinkedList<Car> linkedList = Car.getCarList();
        while (!linkedList.isEmpty()) {
            linkedList.remove(0);
        }
    }
}
