package edu.uprm.cse.datastructures.cardealer.util;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E extends Comparable<E>> implements SortedList<E> {


    private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E> {

        private Node<E> currentNode;
        private Node<E> lastSeenNode;
        private int position = 0;

        public CircularSortedDoublyLinkedListIterator() {
            this.currentNode = (Node<E>) header.getNext();
            this.lastSeenNode = null;
            this.position = 0;
        }

        /**
         * While the current position is less than the current size of the list
         * it means we still have nodes going forward.
         *
         */
        @Override
        public boolean hasNext() {
            return position < currentSize;
        }

        /**
         * If the current position surpasses the current size it means it went back
         * full circle from the last node to the first node.
         *
         */
        public boolean hasPrev() {
            return position > currentSize;
        }

        @Override
        public E next() {
            if (this.hasNext()) {
                E result = this.currentNode.getElement();
                this.currentNode = this.currentNode.getNext();
                this.position++;
                return result;
            }
            else {
                throw new NoSuchElementException();
            }
        }

        public E previous() {
            if (this.hasPrev()) {
                E result = this.currentNode.getPrev().getElement();
                this.lastSeenNode = this.currentNode;
                return result;
            }
            else {
                throw new NoSuchElementException();
            }
        }
    }

    private static class Node<E> {
        private E element;
        private Node<E> next;
        private Node<E> prev;

        public Node() {
            this.element = null;
            this.next = this.prev = null;
        }

        public Node(Node<E> prev, E element, Node<E> next) {
            this.prev = prev;
            this.element = element;
            this.next = next;
        }

        public Node(E element) {
            this.element = element;
        }



        public E getElement() {
            return this.element;
        }
        public void setElement(E element) {
            this.element = element;
        }

        public Node<E> getPrev() {
            return prev;
        }

        public void setPrev(Node<E> prev) {
            this.prev = prev;
        }

        public Node<E> getNext() {
            return next;
        }

        public void setNext(Node<E> next) {
            this.next = next;
        }

    }

    private Node<E> header;
    private Node<E> dummyNode;
    private int currentSize;

    /**
     * Comparator instance object.
     */
    private CarComparator<E> compareCar = new CarComparator<>();


    /**
     * Constructs an empty list with a dummy node pointing to itself in both previous and next references.
     * Basically, a header that points to itself but with a dummy helper node.
     *
     */
    public  CircularSortedDoublyLinkedList() {
        dummyNode = new Node<>();
        // Point dummyNode to itself
        dummyNode.setPrev(dummyNode);
        dummyNode.setNext(dummyNode);
        dummyNode.setElement(null);
        // Points header to itself
        this.header = dummyNode.getNext();
        this.header.setNext(dummyNode);
    }

    /**
     * Constructs an empty list with a dummy node pointing to itself in both previous and next references
     * for the Integer tester.
     *
     * @param integerComparator
     */
    public <Integer> CircularSortedDoublyLinkedList(Integer integerComparator) {
        this.currentSize = 0;
        dummyNode = new Node<>();
        // Point dummyNode to itself
        dummyNode.setPrev(dummyNode);
        dummyNode.setNext(dummyNode);
        dummyNode.setElement(null);
        // Points header to itself
        this.header = dummyNode.getNext();
        this.header.setNext(dummyNode);
    }

    /**
     * Constructs an empty list with a dummy node pointing to itself in both previous and next references
     * for the Integer tester.
     *
     * @param carComparator
     */
    public  CircularSortedDoublyLinkedList(CarComparator carComparator) {
        dummyNode = new Node<>();
        // Point dummyNode to itself
        dummyNode.setPrev(dummyNode);
        dummyNode.setNext(dummyNode);
        dummyNode.setElement(null);
        // Points header to itself
        this.header = dummyNode.getNext();
        this.header.setNext(dummyNode);
    }


    /**
     * This method adds elements to the Doubly linked list and keep the list sorted.
     * Loop until the end of the list (dummyNode will be like the tail)
     * go to the next node if the obj is greater than the current node
     * else add the object before the current node (is less than current node).
     *
     * @param obj to be added to the list in a sorted position
     * @return true if the obj was added successfully to the list
     */
    @Override
    public boolean add(E obj) {
        Node<E> temp = dummyNode.getNext();
        if (obj instanceof Car) {
            while (temp != dummyNode && compareCar.compare(obj, temp.getElement()) > 0) {
                temp = temp.getNext();
            }
            Node<E> newNode = new Node<E>();
            newNode.setElement(obj);
            newNode.setPrev(temp.getPrev());
            newNode.setNext(temp);
            temp.getPrev().setNext(newNode);
            temp.setPrev(newNode);
            this.currentSize++;
            return true;

        }
        // If object is any type other than car type; do this
        else {
            while (temp != dummyNode && obj.compareTo(temp.getElement()) > 0) {
                temp = temp.getNext();
            }
            Node<E> newNode = new Node<E>();
            newNode.setElement(obj);
            newNode.setPrev(temp.getPrev());
            newNode.setNext(temp);
            temp.getPrev().setNext(newNode);
            temp.setPrev(newNode);
            this.currentSize++;
            return true;

        }

    }

    /**
     * Keeps track of the size of the list.
     *
     * @return the current size of the list
     */
    @Override
    public int size() {
        return this.currentSize;
    }


    /**
     * Check if the parameter object is present on the list and remove it.
     *
     * @param obj the object to be removed
     * @return    true if the element was found and thus removed; false otherwise
     */
    @Override
    public boolean remove(E obj) {
        int index = this.firstIndex(obj);
        if (index < 0) {
            return false;
        }
        else {
            this.remove(index);
            return true;
        }
    }

    /**
     * Removes an object present on the given index.
     *
     * @param index position of the element to be removed
     * @return      true if the object was removed from the list; false otherwise
     */
    @Override
    public boolean remove(int index) {
        int i = 0;
        if ((index < 0) || (index >= this.currentSize)) {
            throw new IndexOutOfBoundsException();
        }
        else {
            for (Node<E> currentNode = dummyNode.getNext(); currentNode != dummyNode;
                 currentNode = currentNode.getNext(), i++) {
                if (i == index) {
                    currentNode.getPrev().setNext(currentNode.getNext());
                    currentNode.getNext().setPrev(currentNode.getPrev());
                    this.currentSize--;
                    return true;
                }
            }
            return false;
        }
    }


    /**
     * Remove all the entries of the given object from the list.
     *
     * @param obj objects to be removed
     * @return    in how many positions the object was present
     */
    @Override
    public int removeAll(E obj) {
        int counter = 0;
        int times = 0;
        for (Node<E> temp = this.dummyNode.getNext(); temp != dummyNode; temp = temp.getNext()) {
            if (temp.getElement().equals(obj)) {
                counter++;
            }
        }
        while (times < counter) {
            this.remove(obj);
            times++;
        }
        return counter;
    }


    /**
     * Retrieves the first item from the list.
     *
     * @return the element on the first position
     */
    @Override
    public E first() {
        return dummyNode.getNext().getElement();
    }

    /**
     * Retrieves the last item from the list.
     *
     * @return the element on the last position
     */
    @Override
    public E last() {
        return dummyNode.getPrev().getElement();
    }

    /**
     * Returns the element at the given position.
     *
     * @param index position of the desired element
     * @return      the element at the given position
     */
    @Override
    public E get(int index) {
        if ((index < 0) || (index >= this.currentSize)) {
            throw new IndexOutOfBoundsException();
        }
        Node<E> temp = this.getPosition(index);
        return temp.getElement();
    }


    /**
     * Returns the node at the given position.
     *
     * @param index position of the node to be returned
     * @return      node at the given position
     */
    private Node<E> getPosition(int index) {
        int currentPosition = 0;
        Node<E> temp = this.header.getNext();
        while (temp.getNext() != null && currentPosition != index) {
            temp = temp.getNext();
            currentPosition++;
        }
        return temp;
    }

    /**
     * Empty the list.
     * List size should be 0.
     */
    @Override
    public void clear() {
        while (!this.isEmpty()) {
            this.remove(0);
        }
    }

    /**
     * Check whether the list contains the given item.
     *
     * @param e item to check if it is present on the list
     * @return  true if the item is present; false otherwise
     */
    @Override
    public boolean contains(E e) {
        return firstIndex(e) >= 0;
    }

    /**
     *
     * @return true if list is empty; false otherwise
     */
    @Override
    public boolean isEmpty() {
        return this.currentSize == 0;
    }

    /**
     * Returns the first index position found of the given object.
     *
     * @param e object to check it's first position on the list
     * @return  the first position of the given element; -1 if not present on the list
     */
    @Override
    public int firstIndex(E e) {
        int i = 0;
        for (Node<E> temp = this.dummyNode.getNext(); temp != dummyNode; temp = temp.getNext(), i++) {
            if (temp.getElement().equals(e)) {
                return i;

            }
        }
        return -1; // Not found
    }

    /**
     * Returns the last index position found of the given object.
     *
     * @param e object to check it's last position on the list
     * @return  the last position of the given element; -1 if not present on the list
     */
    @Override
    public int lastIndex(E e) {
        int i = 0, result = -1;
        for (Node<E> temp = this.dummyNode.getNext(); temp != dummyNode; temp = temp.getNext(), i++) {
            if (temp.getElement().equals(e)) {
                result = i;
            }
        }
        return result;
    }

    /**
     * This method receives the position of the car currently on the list that needs to be updated by the
     * update REST method in CarManager class.
     * It updates the old car information with the new parameter car information.
     *
     * @param car the car information to update the old car information
     * @param index the position of the car to be updated
     */
    public void setNew(Car car, int index) {
        int i = 0;
        for (Node<E> temp = this.dummyNode.getNext(); temp != dummyNode; temp = temp.getNext(), i++) {
            if (i == index) {
                temp.setElement((E) car);
            }
        }
        this.toArray();
    }


    /**
     * Converts the Doubly Linked list to an array of objects.
     * @return the array with all the nodes from the doubly linked list
     */
    public Object[] toArray() {
        Object[] result = new Object[this.currentSize];
        for (int i = 0; i < this.currentSize; i++) {
            result[i] = this.get(i);
        }
        return result;
    }


    /**
     * Iterator Constructor.
     */
    @Override
    public Iterator<E> iterator() {
        return new CircularSortedDoublyLinkedListIterator<>();
    }
}
